package main

import "sync"

func main() {
	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(w *sync.WaitGroup) {
			defer w.Done()

			getInstance()
		}(&wg)
	}

	wg.Wait()
}
