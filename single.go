package main

import (
	"log"
	"sync"
)

type (
	single struct{}
)

var (
	instance *single
	lock     = &sync.Mutex{}
)

func getInstance() *single {
	lock.Lock()
	defer lock.Unlock()

	if instance == nil {
		log.Println("Creating instance now.")
		instance = &single{}
	} else {
		log.Println("Single instance already created.")
	}
	return instance
}
